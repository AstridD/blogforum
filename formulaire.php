<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Cinzel|Days+One|Pacifico&display=swap" rel="stylesheet">
    <title>Formulaire</title>
    <style>
* {
    background-color:#EAAEBD;
}
h1 {
    font-family: 'Cinzel';
    color: #E61E9B;
    padding : 25px 0px;
}
form{
    width: 100%;
    /* background-color: #F6AFCD */;
    padding : 5px 0px;
}
/* .myDiv{
    width: 100%;
    margin: 20px;
} */
label{
    display: inline-block;
    min-width: 20%;
    font-family: 'Pacifico';
}
input[type="submit"]{
    color: #E61E9B;
    border-radius: 5px;
    padding: 5px 10px;
    font-size: 14px;
    border: 2px solid #E61E9B;
    font-family: 'Cinzel';
    font-weight: bold;
}
input[type="submit"]:hover{
    background-color: #E61E9B;
    color: #F697AF;
    cursor: pointer;
    box-shadow: 0px 0px 5px 0px #777;
}
    </style>

</head>
<body>
    <div align="center">
        <h1>Formulaire d'inscription</h1>
        <br><br>
        <form method="POST" action="saveProfil.php">
            <table>
                <tr>
                    <td>
                        <label>Prénom : </label>
                    </td>
                    <td>
                        <input type="text" placeholder="Ton prénom" id="prenom" name="prenom"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Mail : </label>
                    </td>
                    <td>
                        <input type="email" placeholder="Ton mail" id="mail" name="mail" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Mot de passe : </label>
                    </td>
                    <td>
                        <input type="text" placeholder="Ton mot de passe" id="pwd" name="pwd" maxlength="8" required/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Age : </label>
                    </td>
                    <td>
                        <input type="number" placeholder="Age" id="age" name="age" min="10" max="35"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Ville : </label>
                    </td>
                    <td>
                        <input type="text" placeholder="Ta ville" id="ville" name="ville"/>
                    </td>
                </tr>
                <tr>
                    <td>
                    <td align="center">
                        <br>
                        <input type="submit" value="Je m'inscris"/>
                    </td>
                </tr>
            </table> 
        </form>
    </div>
    <!-- <h1>FORMULAIRE</h1>
    <form action="saveProfil.php" method="post">
        <div class="myDiv">
            <label for="prenom">Prénom: </label>
            <input type="text" id="prenom" name="prenom">
        </div>
        <div class="myDiv">
            <label for="mail">E-mail: </label>
            <input type="email" id="mail" name="mail" required>
        </div>
        <div class="myDiv">
            <label for="password">Mot de passe: </label>
            <input type="password" name="pwd" maxlength="8" required>
        </div>
        <div class="myDiv">
            <label for="age">Age : </label>
            <input type="number" id="age" name="age" min="10" max="40">
        </div>
        <div class="myDiv">
            <label for="ville">Ville : </label>
            <input type="text" id="ville" name="ville">
        </div>
        <div class="myDiv">
            <input type="radio" id="femme" name="sexe" value="femme">
            <label for="femme">Femme</label>
            <input type="radio" id="homme" name="sexe" value="homme">
            <label for="homme">Homme</label>
            <input type="radio" id="autre" name="sexe" value="autre">
            <label for="autre">Autre</label>
        </div>
        <div class="myDiv" id="submit">
            <input type="submit" value="Envoyer">
        </div>
    </form> -->
</body>
</html>