<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styleBlog.css" />
    <link href="https://fonts.googleapis.com/css?family=Cinzel|Days+One|Pacifico&display=swap" rel="stylesheet">
    <link rel="icon" type="image/png" href="unicorn.png">
    <title>BlogAccueil</title>
<style>
body {
  background-color: #FAB1CA;   
}

.container {
  position: relative;
  text-align: center;
  color: white; 
}

h1 {
  font-family: 'Pacifico', serif; /* police prise sur google fonts */
  color:#F0CD15;
  font-size: 60px;
  position: absolute;
  top: 5%;
  left: 20%;
  transform: translate(-50%, -50%)
}

.button {
  display: block;
  width: 100%;
  /* border: none; */
  background-color: #D03B98;
  padding: 14px 28px;
  font-size: 30px;
  cursor: pointer;
  text-align: center;
  font-family: 'Pacifico', serif;
   -moz-border-radius:10px 0;    /* les lignes 50 et 51 permettent d arrondir l angle en haut à gauche et en bas à droite*/
  -webkit-border-radius:10px 0;
  border-radius:20px 0;
  border: 2px solid #7D216E;
}

form{
   width: 100%;
   background-color: #F6AFCD;
   padding : 5px 0px;
  -moz-border-radius:10px 0;    /* les lignes 50 et 51 permettent d arrondir l angle en haut à gauche et en bas à droite*/
  -webkit-border-radius:10px 0;
   border-radius:20px 0;
   border: 2px solid #E61E9B;
   
}

.myDiv{
    width: 100%;
    margin: 20px;
       
}
label{
    display: inline-block;
    min-width: 20%;
    font-family: 'Pacifico';
    
}
input[type="submit"]{
    color: #E61E9B;
    border-radius: 10px;
    padding: 5px 10px;
    font-size: 14px;
    color: #E1466D;
    border: 2px solid #E61E9B;
    font-family: 'Cenzel';
    -moz-border-radius:10px 0;    /* les lignes 50 et 51 permettent d arrondir l angle en haut à gauche et en bas à droite*/
    -webkit-border-radius:10px 0;
    border-radius:10px 0;
}
input[type="submit"]:hover{
    background-color: #E61E9B;
    color: #F6AEDB;
    cursor: pointer;
    /* box-shadow: 0px 0px 5px 0px #777; */
}
</style>
</head>
<body>

    <div class="container">
        <img src="licorne3.jpg" alt="fleurs" width="1320" height="500">

<H1>RaconteTaLife.com</H1>
    <CENTER><button class="button" onclick="window.location.href = 'editor.html';">Forum</button></CENTER><br>
    <CENTER><button class="button" onclick="window.location.href = 'formulaire.php';">Inscription</button></CENTER><br>
    <!-- <CENTER><button class="button" onclick="window.location.href = 'connexion.php';">Se connecter</button></CENTER><br> -->
    
    <form action="connexion.php" method="post">
 <!--        <div class="myDiv">
            <label for="prenom">Prénom: </label>
            <input type="text" id="prenom" name="prenom">
        </div> -->
        <div class="myDiv">
            <label for="mail">E-mail: </label>
            <input type="email" id="mail" name="mail" placeholder="mail">
        </div>
        <div class="myDiv">
            <label for="password">Mot de passe: </label>
            <input type="password" name="pwd" maxlength="8" placeholder="password">
            </div>
<!--         <div class="myDiv">
            <label for="age">Age : </label>
            <input type="number" id="age" name="age">
        </div>
        <div class="myDiv">
            <label for="ville">Ville : </label>
            <input type="text" id="ville" name="ville">
        </div> -->
        <div class="myDiv" id="submit">
            <input type="submit" name=formConnexion value="se connecter"> 
            
        </div>
    </form>
</div>
    <div>
        <?php include ('select.php'); ?>
    </div>
</body>
</html>