<?php
session_start();

$bdd = new PDO("mysql:host=localhost;dbname=blog", "astrid", "jaika34");

if(isset($_GET['id']) AND $_GET['id']>0){
    $getid = intval($_GET['id']); //converti ce que l utilisateur met en nombre
    $requser = $bdd->prepare('SELECT * FROM user WHERE id = ?');
    $requser->execute(array($getid));
    $userInfo = $requser->fetch(); //recuperer les infos renseignées par le user

}

?>

<html>
    <head>
        <title>Profil</title>
        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Cinzel|Days+One|Pacifico&display=swap" rel="stylesheet">

        <style>
            * {
  background-color: #FAB1CA; 
  font-family: 'Pacifico';
  font-size: 30px;
  color: #E61E9B;
  padding : 5px 0px;
                
              }
        
        </style>
    </head>
    <body>
        <div align="center">
            <h2>Profil de <?php echo $userInfo ['prenom']?></h2>
            <br><br>
            Prénom = <?php echo $userInfo ['prenom']?>
            <br>
            Mail = <?php echo $userInfo ['mail']?>
            <br>
            Age = <?php echo $userInfo ['age']." ans"?>
            <br>
            Ville = <?php echo $userInfo ['ville']?>
            <br>
            <?php
            if(isset($_SESSION['id']) AND $userInfo['id'] == $_SESSION['id']) // ISSET DE SESSION ID POUR EVITER DE VISITER LE PROFIL SANS CONNEXION
            {
            ?>
            <a href="#">Editer mon profil</a>
            <a href="deconnexion.php">Se déconnecter</a>
            <?php
            }
            ?>
        </div>
    </body>
</html>